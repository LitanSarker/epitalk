import { Component, OnInit, Inject } from '@angular/core';
import { EpidemicService } from "../services/epidemic.service";
import { Epidemic } from '../shared/epidemic';
import { flyInOut, expand } from "../animations/app.animation";

@Component({
  selector: 'app-epidemic',
  templateUrl: './epidemic.component.html',
  styleUrls: ['./epidemic.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    expand()
  ]
})
export class EpidemicComponent implements OnInit {
  epidemics: Epidemic[];
  errMess: string;

  constructor(private epidemicService: EpidemicService,
    @Inject('BaseURL') public BaseURL) { }

  ngOnInit(): void {
    this.epidemicService.getEpidemics()
    .subscribe((epidemics) => this.epidemics = epidemics,
    errmess => this.errMess = <any>errmess);
  }

}
