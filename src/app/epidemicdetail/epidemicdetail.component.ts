import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { Params, ActivatedRoute } from "@angular/router";
import { Location } from "@angular/common";
import { Epidemic } from '../shared/epidemic';
import { EpidemicService } from '../services/epidemic.service';
import { switchMap } from "rxjs/operators";
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Comment } from "../shared/comment";
import { visibility, flyInOut, expand } from "../animations/app.animation";


@Component({
  selector: 'app-epidemicdetail',
  templateUrl: './epidemicdetail.component.html',
  styleUrls: ['./epidemicdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
  },
  animations: [
    flyInOut(),
    visibility(),
    expand()
  ]
})
export class EpidemicdetailComponent implements OnInit {
  @ViewChild('cform') commentFormDirective;
  epidemic: Epidemic;
  epidemicIds: string[];
  prev: string;
  next: string;
  errMess: string; 
  comment: Comment;
  commentForm: FormGroup;  
  epidemiccopy: Epidemic;
  visibility= 'shown';

  formErrors = {
    'author': '',
    'comment': ''
  };

  validationMessages = {
    'author': {
      'required': 'Name is required.',
      'minlength': 'Name must be at least 2 characters long.',
      'maxlength': 'Name can not be more than 25 characters long.'
    },
    'comment': {
      'required': 'Comment is required.',
      'minlength':  'Comment must be at least 1 characters long.'
    }
  };


  constructor(private fb: FormBuilder ,private epidemicService: EpidemicService,
     private location: Location
    ,private route: ActivatedRoute
    ,@Inject('BaseURL') public BaseURL) { 
      
    }

  ngOnInit(): void {
    this.createForm();

    this.epidemicService.getEpidemicIds()
    .subscribe((epidemicIds) => this.epidemicIds = epidemicIds);

    this.route.params
      .pipe(switchMap((params: Params) => { this.visibility = 'hidden'; return this.epidemicService.getEpidemic(params['id']);}))
      .subscribe(epidemic => { this.epidemic = epidemic; this.epidemiccopy = epidemic; this.setPrevNext(epidemic.id); this.visibility='shown'},
        errmess => this.errMess = <any>errmess );

    
  }

  createForm()  {
    this.commentForm = this.fb.group({
      author: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      comment: ['', [Validators.required, Validators.minLength(1)] ],
      rating: 5
    });

    this.commentForm.valueChanges
    .subscribe(data => this.onValueChanged(data));

    this.onValueChanged();
  }

  onValueChanged(data?: any): void {
    if (!this.commentForm) { return; }
    const form = this.commentForm;
    for (const field in this.formErrors){
      if (this.formErrors.hasOwnProperty(field)){
        //clear previous error message (if any)
        this.formErrors[field] = '';
        const control = form.get(field);
        if(control && control.dirty &&  !control.valid){
          const messages = this.validationMessages[field];
          for (const key in control.errors){
            if(control.errors.hasOwnProperty(key)){
              this.formErrors[field] += messages[key] + ' ';
            }
          }
        }
      }
    }
    this.comment = form.value;
  }

  onSubmit(){ 
    this.comment = this.commentForm.value;
    this.comment.date = new Date().toISOString();
    console.log(this.comment);
    this.epidemiccopy.comments.push(this.comment);
    this.epidemicService.putEpidemic(this.epidemiccopy)
      .subscribe(epidemic => {
        this.epidemic = epidemic; this.epidemiccopy = epidemic;
      },
      errmess => { this.epidemic = null; this.epidemiccopy = null; this.errMess = <any>errmess; });
   
    this.commentFormDirective.resetForm();
    //this.comment = null;
    this.commentForm.reset({
      author: '',
      rating: 5,
      comment: '',
    });
    
    
  }

  setPrevNext(epidemicId: string){
    const index = this.epidemicIds.indexOf(epidemicId);
    this.prev = this.epidemicIds[(this.epidemicIds.length + index - 1) % this.epidemicIds.length];   

    this.next = this.epidemicIds[(this.epidemicIds.length + index + 1) % this.epidemicIds.length];   
  }

  goBack(): void {
    this.location.back();
  }

}
