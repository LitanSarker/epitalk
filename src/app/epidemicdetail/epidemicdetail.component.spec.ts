import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpidemicdetailComponent } from './epidemicdetail.component';

describe('EpidemicdetailComponent', () => {
  let component: EpidemicdetailComponent;
  let fixture: ComponentFixture<EpidemicdetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpidemicdetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpidemicdetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
