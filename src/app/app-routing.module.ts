import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { EpidemicComponent } from './epidemic/epidemic.component';
import { ContactComponent } from './contact/contact.component';
import { EpidemicdetailComponent } from './epidemicdetail/epidemicdetail.component';


const routes: Routes = [
  {path: '',redirectTo: '/epidemic', pathMatch:'full'},
  {path: 'home', component: HomeComponent},
  {path: 'aboutus', component: AboutComponent},
  {path: 'epidemic', component: EpidemicComponent},
  {path: 'contactus', component: ContactComponent},
  {path: 'epidemicdetail/:id', component: EpidemicdetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
