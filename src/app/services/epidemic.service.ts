import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from "rxjs";
import { map, catchError } from "rxjs/operators";
import { Epidemic } from "../shared/epidemic";
import { baseURL } from "../shared/baseurl";
import { ProcessHTTPMsgService } from "./process-httpmsg.service";

@Injectable({
  providedIn: 'root'
})
export class EpidemicService {

  constructor(private http: HttpClient,
    private processHTTPMsgService: ProcessHTTPMsgService) { }

  getEpidemics(): Observable<Epidemic[]> {
    return this.http.get<Epidemic[]>(baseURL + 'epidemics')
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getEpidemic(id: string): Observable<Epidemic> {
    return this.http.get<Epidemic>(baseURL + 'epidemics/' + id)
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getRunningEpidemic(): Observable<Epidemic> {
    return this.http.get<Epidemic[]>(baseURL + 'epidemics?isrunning=ture')
    .pipe(map(epidemics => epidemics[0]))
    .pipe(catchError(this.processHTTPMsgService.handleError));
  }

  getEpidemicIds(): Observable<string[] | any> {
    return this.getEpidemics().pipe(map(epidemics => epidemics.map(epidemic => epidemic.id)))
    .pipe(catchError(error => error));
  }

  putEpidemic(epidemic: Epidemic): Observable<Epidemic> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
    return this.http.put<Epidemic>(baseURL + 'epidemics/' + epidemic.id, epidemic, httpOptions)
      .pipe(catchError(this.processHTTPMsgService.handleError));

  }


}
