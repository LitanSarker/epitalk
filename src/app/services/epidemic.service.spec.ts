import { TestBed } from '@angular/core/testing';

import { EpidemicService } from './epidemic.service';

describe('EpidemicService', () => {
  let service: EpidemicService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EpidemicService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
